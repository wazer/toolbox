# File Sharing Sites

- [**Bunkr**](https://bunkr.is)
- [**CyberDrop**](https://cyberdrop.me)
- [**Fireload**](https://fireload.com)
- [**GoFile**](http://gofile.io)
- [**SwissTransfer**](https://www.swisstransfer.com/en-us)
