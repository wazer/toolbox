# Passwords
Password Managers, 2FA apps

- [**Password Strength Chart**](https://hivesystems.io/blog/are-your-passwords-in-the-green)
- [**Password Manager Emergency Kit**](https://passwordbits.com/password-manager-emergency-sheet) - _If you use 1Password, it comes with it's own emergency kit._

# Cloud Based Password Managers

- [**1Password**](https://1password.com) ✓
- [**Bitwarden**](https://bitwarden.com) ✓
- [**Dashlane**](https://dashlane.com)
- [**Roboform**](https://roboform.com)

## Self Hosted Password Managers

- [**KeepassXC**](https://keepassxc.org)
- [**Vaultwarden**](https://github.com/dani-garcia/vaultwarden) - _Self Hosted Bitwarden_

# 2FA

- [**2fa.directory**](https://2fa.directory/int) - _See which services support what type of 2FA (Email, SMS, software / hardware token)_
- [**Authy**](https://authy.com) ✓ - _All Platforms_
- [**2FAS**](https://2fas.com) - _Android & iOS_
- [**Tofu**](https://tofuauth.com) - _iOS_
- [**Raivo OTP**](https://github.com/raivo-otp/ios-application#readme) ✓ - _iOS_
- [**OTP Auth**](https://apps.apple.com/ca/app/otp-auth/id659877384) - _iOS_
- [**Aegis**](https://getaegis.app) ✓ - _Android_
