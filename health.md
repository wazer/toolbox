# Health

Health / Workout Trackers, ...

# Table of Contents

- 

# Period / Cycle Tracking Apps
Private / End To End Encrypted Period Tracking Apps

### iOS
- [**Apple Health Period Tracker**](https://support.apple.com/en-us/HT210407)
- [**Stardust Period Tracker**](https://thestardustapp.com) - Android Coming Soon

### Android
- [**Drip**](https://bloodyhealth.gitlab.io) - Open Source

# Fitness / Workout Apps

### iOS & Android
- [**MapMyFitness**](https://myfitnesspal.com)
- [**Nike Training Club**](https://www.nike.com/ntc-app)
- [**Nike Run Club**](https://www.nike.com/nrc-app)

# Sleep Tracking

### iOS
- [AutoSleep](https://apps.apple.com/us/app/autosleep-tracker-per-watch/id1164801111) - 4.99
# General Health Apps

### iOS & Android

### iOS
- [**Heart Reports**](https://apps.apple.com/us/app/heart-reports/id1448243870) - Generate Health Reports (_Free / 4.99 for unlimited exports_)

### Android

