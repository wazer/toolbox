# Software Downloading

[Check the torrenting sites section if you want to torrent software](torrenting.md)

# Software Piracy

- [**FileCR**](https://filecr.com) - Windows / Mac / Android
- [**LRepacks**](https://lrepacks.net)
- [**Sanet**](https://sanet.st)
- [**NSane**](https://nsaneforums.com)
