# Toolbox
Toolbox is a list of websites, applications and tools that can be found across the internet. Toolbox is heavily inspired by FREEMEDIAHECKYEAH. 

Toolbox is still a work in progress, but new sites are being added every day. 

- [Github](https://github.com/Toolbox-List/Toolbox)
- [Gitlab](https://gitlab.com/toolbox-resources/toolbox)
- [Download Toolbox (.md files)](https://gitlab.com/toolbox-resources/toolbox/-/archive/master/toolbox-master.zip)


# Contents
## Privacy / Security / Ad-Blocking

- [**AdBlock**](privacy-security/adblock.md)
- [**VPN**](privacy-security/vpn.md)
- [**Anti Virus**](privacy-security/anti-virus.md)
- [**Password Tools**](privacy-security/passwords.md)
- [**Encryption Tools**](privacy-security/encryption.md)

## Gaming
- [**Game Piracy Sites**](gaming/game-piracy.md)

## Reading

## Movies / TV / Sports / Anime
- [**Movie / TV / Anime Streaming**](streaming/movie-streaming.md#movie-tv-show-anime-streaming)
- [**Sports**](streaming/sports.md)

## Piracy
- [**Movie / TV / Anime Piracy Sites**](streaming/movie-streaming.md#movie-tv-show-anime-streaming)
- [**Sports**](streaming/sports.md)
- [**Game Piracy Sites**](gaming/game-piracy.md)
- [**Software Piracy**](software.md)

## Misc
- [**Torrenting**](torrenting.md)
- [**Health Tools**](health.md)
- [**Software**](software.md)
- [**File Sharing Sites**](file-sharing.md)
