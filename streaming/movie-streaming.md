# Movie / TV Show / Anime Streaming
Sites to watch movies, tv shows, anime. Sites to download subtitles, other streaming tools that may be useful. 

Pro Tip: Use [uBlock Origin](https://github.com/gorhill/uBlock#installation) on these sites or you are going to have a terrible time.

Pro Tip: Use `CTRL + F` to search for what you want. Example: "anime" or the sport you are looking for.
`CMD + F` on mac.

# Movies / TV  / Anime Streaming

- [**Soap2Day**](https://soapgate.org) ✓
- [**Oligopoly**](https://olgply.com) ✓
- [**PrivateHD**](https://privatehd.xyz)
- [**OnionPlay**](https://onionplay.network) ✓
- [**Trailers.to**](https://trailers.to)
- [**C1ne**](https://c1ne.co)
- [**FMovies**](https://fmovies.name)
- [**oSee**](https://osee.in)
- [**Ask4Movie**](https://ask4movie.me)
- [**GoStream**](https://gostream.site)
- [**VexStream**](https://vexmovies.org)
- [**LookMovie**](https://lookmovie2.to) -- [_Mirrors_](https://proxymirrorlookmovie.github.io) ✓
- [**MoviesCF**](https://movies.cf/Landing)
- [**123-Movies.buzz**](https://123-movies.buzz)
- [**PeraMovies**](https://peramovies.club)
- [**M4uFree**](https://m4ufree.com)
- [**RainierLand**](https://rainierland.to)
- [**Series9**](https://series9.la)
- [**YesMovies**](https://yesmovies.ag)
- [**SolarMovie**](https://solarmovie.to)
- [**123Movie**](https://0123movie.net)
- [**PutLocker**](https://putlocker.vip)
- [**CinemaShack**](https://cinemashack.co)
- [**FshareTV**](https://fsharetv.co)
- [**SockShare**](https://sockshare.ac)
    - [_SockShare Clones_](https://gitlab.com/toolbox-resources/lists/-/raw/main/sockshare.ac.txt)
- [**123MoviesFree**](https://ww1.123moviesfree.net)
- [**123MoviesGoTo**](https://123moviesgoto.com)
- [**StreamLord**](http://streamlord.com)
- [**Cine**](https://cine.to)
- [**GooJara**](https://www.goojara.to/)
- [**TheFlixer](https://theflixer.tv)
- [**MoviesOnlineFree**](https://moviesonlinefree.net)
- [**Flixtor.Stream**](https://flixtor.stream)
- [BeMovies](https://bemovies.co)
